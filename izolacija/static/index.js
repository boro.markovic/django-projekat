require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const nodemailer = require('nodemailer')

const app = express()

var path = require('path')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static(path.join(__dirname, 'dist')))

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'))
})

app.get('/polyurea', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/polyurea.html'))
})

app.get('/polyurethane', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/polyurethane.html'))
})

app.get('/polyurethane', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/polyurethane.html'))
})
app.get('/protivpozarnazastita', function (req, res) {
  res.sendFile(path.join(__dirname + '/dist/protivpozarnazastita.html'))
})

// Use https://realfavicongenerator.net/ instead
// app.get('/favico.ico', (req, res) => {
//     res.sendFile("/dist/favicon.ico");
// })

// Not needed, static path is set to - dist folder
// app.get('/google12cfc68677988bb4.html', function (req, res) {
//   res.sendFile(__dirname+"/google12cfc68677988bb4.html");
//  })

app.post('/', function (req, res) {
  var name = req.body['name']
  var email = req.body['email']
  var phone = req.body['phone']
  var message = req.body['message']

  sendEmail(
    email,
    'mirko@msolutions.me',
    'Nova poruka od: ' + name,
    'Tekst poruke: ' +
      message +
      '\n' +
      '\n' +
      ' Telefon: ' +
      phone +
      '\n' +
      '\n' +
      'Email: ' +
      email
  )
})

function sendEmail (from, to, subject, text) {
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS
    }
  })

  var mailOptions = {
    from: from,
    to: to,
    subject: subject,
    text: text
  }

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error)
    } else {
      console.log('Email sent: ' + info.response)
    }
  })
}

app.listen(8080)