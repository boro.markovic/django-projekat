# pylint: disable=missing-class-docstring
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import Post,CaseStoryPost

# Create your views here.


def base(request):
    return render(request,'base.html',{})

def mapa(request):
    return render(request,'map.html',{})
    
class CaseStoryView(ListView):
    model = CaseStoryPost
    template_name = 'case_story.html'

class StoryView(DetailView):
    model = CaseStoryPost
    template_name = 'story.html'

class HomeView(ListView):
    model = Post
    template_name = 'home.html'    
