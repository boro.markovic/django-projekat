from pydoc import describe
from pyexpat import model
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=255)
    title_tag= models.CharField(max_length=255)

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('home')


class CaseStoryPost(models.Model):
    title = models.CharField(max_length=255)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=255)
    body = models.TextField(default="")

    def __str__(self):
        return self.title + ' | ' + str(self.author)

