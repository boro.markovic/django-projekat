
from unicodedata import name
from django.urls import path, include
from .views import base
from .views import CaseStoryView, StoryView,HomeView


urlpatterns = [
    path('',base,name='base'),
    path('case_story', CaseStoryView.as_view(), name="case_story"),
    path('story/<int:pk>', StoryView.as_view(), name="story_details"),
]
from django.urls import path
from .views import mapa,HomeView

urlpatterns = [
    path('mapa',mapa, name = 'mapa'),
    path('', HomeView.as_view(), name="home"),
    path('i18n/', include('django.conf.urls.i18n')),
]
