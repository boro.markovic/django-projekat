from django.contrib import admin
from .models import Post,CaseStoryPost

# Register your models here.
admin.site.register(Post)
admin.site.register(CaseStoryPost)