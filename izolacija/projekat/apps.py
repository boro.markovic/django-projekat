from django.apps import AppConfig


class ProjekatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'projekat'
